package ru.astafev.springboottest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = "ru.astafev.springboottest.domain")
@EnableJpaRepositories(basePackages = "ru.astafev.springboottest.repositories")
@Slf4j
public class Application {
    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(Application.class);

        springApplication.setShowBanner(false);

        springApplication.run(args);
    }
}
