package ru.astafev.springboottest.sortmethods;

import java.util.*;

public class LSDRadixSortImpl implements ISortMethod<Integer> {
    @Override
    public void sort(List<Integer> list) {
        @SuppressWarnings("unchecked")
        Queue<Integer>[] buckets = new Queue[10];
        for (int i = 0; i < 10; i++)
            buckets[i] = new LinkedList<>();

        boolean sorted = false;
        int expo = 1;

        while (!sorted) {
            sorted = true;

            for (int item : list) {
                int bucket = (item / expo) % 10;
                if (bucket > 0) sorted = false;
                buckets[bucket].add(item);
            }

            expo *= 10;
            int index = 0;

            for (Queue<Integer> bucket : buckets)
                while (!bucket.isEmpty())
                    list.set(index++, bucket.remove());
        }
    }
}

