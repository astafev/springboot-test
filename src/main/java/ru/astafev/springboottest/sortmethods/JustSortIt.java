package ru.astafev.springboottest.sortmethods;

import java.util.Collections;
import java.util.List;

public  class JustSortIt <T extends Comparable<? super T>> implements ISortMethod<T> {
    @Override
    public void sort(List<T> unsorted) {
        Collections.sort(unsorted);
    }
}
