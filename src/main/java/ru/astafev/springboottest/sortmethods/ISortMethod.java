package ru.astafev.springboottest.sortmethods;

import java.util.Collection;
import java.util.List;

public interface ISortMethod<T> {

    void sort(List<T> unsorted);

    default List<T> sortedList(Collection<T> unsortedCollection) {
        throw new UnsupportedOperationException("not implemented");
    };

}
