package ru.astafev.springboottest.sortmethods;

import java.util.List;

public class BubbleSortImpl implements ISortMethod<Integer> {
    @Override
    public void sort(List<Integer> unsorted) {
        for (int i = 0; i < unsorted.size() - 1; i++)
            for (int j = 0; j < unsorted.size() - i - 1; j++)
                if (unsorted.get(i) > unsorted.get(i + 1)) {
                    Integer a = unsorted.get(i);
                    unsorted.set(i, unsorted.get(i + 1));
                    unsorted.set(i + 1, a);
                }
    }
}
