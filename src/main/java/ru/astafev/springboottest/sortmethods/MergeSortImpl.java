package ru.astafev.springboottest.sortmethods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MergeSortImpl implements ISortMethod<Integer> {

    @Override
    public void sort(List<Integer> unsorted) {
        Integer[] tmp = new Integer[unsorted.size()];
        mergeSort(unsorted, tmp, 0, unsorted.size() - 1);
    }

    private static void mergeSort(List<Integer> a, Integer[] tmp, int left, int right) {
        if (left < right) {
            int center = (left + right) / 2;
            mergeSort(a, tmp, left, center);
            mergeSort(a, tmp, center + 1, right);
            merge(a, tmp, left, center + 1, right);
        }
    }

    private static void merge(List<Integer> a, Integer[] tmp, int left, int right, int rightEnd) {
        int leftEnd = right - 1;
        int k = left;
        int num = rightEnd - left + 1;

        while (left <= leftEnd && right <= rightEnd)
            if (a.get(left) <= a.get(right))
                tmp[k++] = a.get(left++);
            else
                tmp[k++]= a.get(right++);

        while (left <= leftEnd)    // Copy rest of first half
            tmp[k++] = a.get(left++);

        while (right <= rightEnd)  // Copy rest of right half
            tmp[k++] = a.get(right++);

        // Copy tmp back
        for (int i = 0; i < num; i++, rightEnd--)
            a.set(rightEnd, tmp[rightEnd]);
    }

}
