package ru.astafev.springboottest.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.astafev.springboottest.domain.Authority;
import ru.astafev.springboottest.domain.Order;

import java.util.List;

@Repository
public interface AuthoritiesRepository extends CrudRepository<Authority, Integer> {
}
