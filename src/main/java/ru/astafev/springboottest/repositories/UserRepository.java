package ru.astafev.springboottest.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.astafev.springboottest.domain.User;

@Repository
//@RepositoryRestResource(path = "/rest/user")
public interface UserRepository extends PagingAndSortingRepository<User, Integer> {
    User findOneByUsername(String username);
}
