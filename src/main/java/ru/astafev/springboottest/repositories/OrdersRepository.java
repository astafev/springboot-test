package ru.astafev.springboottest.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.astafev.springboottest.domain.Order;
import ru.astafev.springboottest.domain.User;

import java.util.List;

@Repository
public interface OrdersRepository extends PagingAndSortingRepository<Order, Integer> {

    public List<Order> findByUsername(String username);
}