package ru.astafev.springboottest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import ru.astafev.springboottest.domain.Authority;
import ru.astafev.springboottest.domain.User;
import ru.astafev.springboottest.domain.UserRole;
import ru.astafev.springboottest.repositories.AuthoritiesRepository;
import ru.astafev.springboottest.repositories.UserRepository;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.Collections;
import java.util.HashSet;

@Controller
@RequestMapping("/auth/")
public class AuthController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthoritiesRepository authoritiesRepository;

    @RequestMapping(value = "register", method = RequestMethod.GET)
    public String registrationPage() {
        return "register";
    }

    @RequestMapping(value = "register", method = RequestMethod.POST)
    public String registerNewUser(@Valid User user) {
        Authority userRole = new Authority();
        userRole.setAuthority(UserRole.ROLE_USER);
        userRole.setUser(user);
        user.setAuthorities(new HashSet<>());
        user.getAuthorities().add(userRole);
        userRepository.save(user);
//        authoritiesRepository.save(userRole);
        return "redirect:register";
    }

    @RequestMapping(value = "list", method = RequestMethod.GET)
    @RolesAllowed("ROLE_ADMIN")
    public ModelAndView listAllUsers() {
        ModelAndView modelAndView = new ModelAndView("list-users");
        modelAndView.addObject("users", userRepository.findAll());
        return modelAndView;
    }

}
