package ru.astafev.springboottest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.astafev.springboottest.domain.Order;
import ru.astafev.springboottest.repositories.OrdersRepository;
import ru.astafev.springboottest.repositories.UserRepository;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.io.IOException;

@Controller
@RequestMapping("/orders")
@RolesAllowed("ROLE_USER")
public class OrdersController {

    @Autowired
    OrdersRepository ordersRepository;

    @Autowired
    UserRepository usersRepository;

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String newOrdersPage() {
        return "order/new-order";
    }

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String createOrder(@Valid Order order,
                              @AuthenticationPrincipal org.springframework.security.core.userdetails.User u) throws IOException {
        order.setUser(
                usersRepository.findOneByUsername(u.getUsername())
        );
        order.fillSortedArray();
        ordersRepository.save(order);
        return "order/new-order";
    }

    @RequestMapping(value = "list")
    public ModelAndView listOrdersPage(
            @AuthenticationPrincipal org.springframework.security.core.userdetails.User u
    ) {
        ModelAndView modelAndView = new ModelAndView("order/orders-list");

        modelAndView.addObject(
                "orders", ordersRepository.findByUsername(u.getUsername()));

        return modelAndView;
    }

    @RequestMapping(value = "all")
    @RolesAllowed("ROLE_ADMIN")
    public ModelAndView allOrders() {
        ModelAndView modelAndView = new ModelAndView("order/orders-list");

        modelAndView.addObject(
                "orders", ordersRepository.findAll());

        return modelAndView;
    }

}
