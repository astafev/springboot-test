package ru.astafev.springboottest.domain;

import ru.astafev.springboottest.sortmethods.*;

public enum SortType {
    BubbleSort(new BubbleSortImpl()),
    MergeSort(new MergeSortImpl()),
    LSDRadixSort(new LSDRadixSortImpl());

    final ISortMethod<Integer> impl;

    SortType() {
        this(new JustSortIt<>());
    }

    SortType(ISortMethod<Integer> impl) {
        this.impl = impl;
    }
}
