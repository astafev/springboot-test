package ru.astafev.springboottest.domain;

public enum UserRole {
    ROLE_ADMIN, ROLE_USER;
}
