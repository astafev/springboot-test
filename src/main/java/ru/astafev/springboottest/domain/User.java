package ru.astafev.springboottest.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Entity(name="users")
public class User {

    @Id
    @Size(min = 1, max = 50)
    String username;

    @Size(min = 1, max = 50)
    String password;

    Boolean enabled = true;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    Set<Authority> authorities;
}
