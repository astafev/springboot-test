package ru.astafev.springboottest.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.util.ArrayList;

@Data
@Entity(name = "orders")
@NamedQuery(
        name="Order.findByUsername",
        query = "select o from orders o join o.user u where u.username = ?1"
)
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;

    @Size(min = 3, max = 1024)
    @Column(length = 1024)
    String originalArray;

    @Column(length = 1024)
    String sortedArray;

    @NotNull
    @Enumerated(EnumType.STRING)
    SortType type;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    User user;


    static ObjectMapper mapper = new ObjectMapper();
    public void fillSortedArray() throws IOException {
        if(originalArray == null || type==null)
            throw new IllegalStateException();
        ArrayList<Integer> list = mapper.readValue(
                originalArray,
                mapper.getTypeFactory().constructCollectionType(ArrayList.class, Integer.class)
        );

        type.impl.sort(list);

        sortedArray = mapper.writeValueAsString(list);
    }
}
