package ru.astafev.springboottest.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name="authorities")
public class Authority {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;

    @ManyToOne
    @JoinColumn(name = "username", referencedColumnName = "username")
    User user;

    @Enumerated(EnumType.STRING)
    UserRole authority;


    @Override
    public String toString() {
        return "Authority{" +
                "id=" + id +
                ", authority=" + authority +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Authority)) return false;

        Authority authority1 = (Authority) o;

        if (authority != authority1.authority) return false;
        if (id != null ? !id.equals(authority1.id) : authority1.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (authority != null ? authority.hashCode() : 0);
        return result;
    }
}
